import React, { useContext } from 'react';
import { Route, BrowserRouter as Router, Switch, useLocation,Redirect } from 'react-router-dom';
import AccountPage from './feature/account/components';
import LoginPage from './feature/account/components/login-page/login-page';
import LandingPage from './feature/landing-page/landing-page';
import { AuthContext } from './feature/shared/contexts/auth.context';
import WebLayout from './feature/shared/web-layout/web-layout';

const GuestRoute = ({ component, ...rest }) => {
  return (
    <Route {...rest} render={() => <WebLayout>{component}</WebLayout>}></Route>
  );
};

const AuthenticatedRoute = ({ component, ...rest }) => {
  const auth = useContext(AuthContext);
  const location = useLocation();

  return (
    <Route
      {...rest}
      render={() =>
        auth.isAuthenticated() ? (
          <WebLayout>{component}</WebLayout>
        ) : (
          <Redirect
            to={{
              pathname: '/account/login',
              state: { from: location.pathname },
            }}
          />
        )
      }
    ></Route>
  );
};

const App = () => {
  return (
    <Router>
      <Switch>
        <Route exact={true} path="/">
          <LandingPage />
        </Route>
        <Route path="/account">
          <AccountPage />
        </Route>
        <GuestRoute path="/login" component={<LoginPage />} />
      </Switch>
    </Router>
  );
};

export default App;
