export interface User {
  id?: string;
  firstName?: string;
  lastName?: string;
  fullName?: string;
  email?: string;
  phone?: string;
  password?: string;
  isBlocked?: boolean;
  isActive?: boolean;
  roles?: string[];
  profilePicture?: string;
  clientId?: string;
}
