import './register-page.module.scss';

/* eslint-disable-next-line */
export interface RegisterPageProps {}

export function RegisterPage(props: RegisterPageProps) {
  return (
    <div>
      <h1>Welcome to register-page!</h1>
    </div>
  );
}

export default RegisterPage;
