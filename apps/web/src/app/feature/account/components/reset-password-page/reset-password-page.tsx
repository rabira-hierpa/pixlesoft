import './reset-password-page.module.scss';

/* eslint-disable-next-line */
export interface ResetPasswordPageProps {}

export function ResetPasswordPage(props: ResetPasswordPageProps) {
  return (
    <div>
      <h1>Welcome to reset-password-page!</h1>
    </div>
  );
}

export default ResetPasswordPage;
