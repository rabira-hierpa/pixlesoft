import { render } from '@testing-library/react';

import ConfirmationPage from './confirmation-page';

describe('ConfirmationPage', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<ConfirmationPage />);
    expect(baseElement).toBeTruthy();
  });
});
