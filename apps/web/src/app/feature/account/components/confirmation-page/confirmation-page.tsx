import './confirmation-page.module.scss';

/* eslint-disable-next-line */
export interface ConfirmationPageProps {}

export function ConfirmationPage(props: ConfirmationPageProps) {
  return (
    <div>
      <h1>Welcome to confirmation-page!</h1>
    </div>
  );
}

export default ConfirmationPage;
