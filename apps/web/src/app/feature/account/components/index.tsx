import React from 'react';
import { ErrorFallback } from '@pixelsoft/react/core';
import { ErrorBoundary } from 'react-error-boundary';
import {
  Redirect,
  Route,
  Switch,
  useLocation,
  useRouteMatch,
} from 'react-router-dom';
import LoginPage from './login-page/login-page';
import RegisterPage from './register-page/register-page';
import ConfirmationPage from './confirmation-page/confirmation-page';
import ForgotPasswordPage from './forgot-password-page/forgot-password-page';
import ResetPasswordPage  from './reset-password-page/reset-password-page';

function AccountPage() {
  const { path, url } = useRouteMatch();
  const location = useLocation();

  return (
    <ErrorBoundary FallbackComponent={ErrorFallback}>
      <Switch>
        <Route exact path={`${path}/`}>
          <Redirect to={`${path}/login`} />
        </Route>
        <Route exact path={`${path}/login`}>
          <LoginPage />
        </Route>
        <Route exact path={`${path}/register`}>
          <RegisterPage />
        </Route>
        <Route exact path={`${path}/activate/:id/:token`}>
          <ConfirmationPage />
        </Route>
        <Route exact path={`${path}/forgot`}>
          <ForgotPasswordPage />
        </Route>
        <Route exact path={`${path}/forgotPassword/:id/:token`}>
          <ResetPasswordPage />
        </Route>
      </Switch>
    </ErrorBoundary>
  );
}

export default AccountPage;
