import { constants, storage } from '@pixelsoft/react/core';
import React, { createContext, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { User } from '../../models/user';

const AuthContext = createContext<AuthContextProps>(undefined);

const { Provider } = AuthContext;

interface AuthContextProps {
  authState?: AuthState;
  setAuthState?: (authInfo: unknown) => void;
  logout?: () => void;
  isAuthenticated?: () => boolean;
  isAdmin?: () => boolean;
}

interface AuthState {
  token?: string;
  user?: User;
  expiresAt?: string;
}

const AuthProvider = ({ children }) => {
  const history = useHistory();

  const token = localStorage.getItem(constants.TOKEN);
  const user = localStorage.getItem(constants.USER);
  const expiresAt = localStorage.getItem(constants.EXPIRES_AT);

  const [authState, setAuthState] = useState<AuthState>({
    token,
    expiresAt,
    user: user ? JSON.parse(user) : {},
  });

  const setAuthInfo = (authInfo: AuthState) => {
    if (authInfo?.token) {
      localStorage.setItem(constants.USER, JSON.stringify(authInfo.user));
      localStorage.setItem(constants.TOKEN, JSON.stringify(authInfo.token));
      localStorage.setItem(constants.EXPIRES_AT, authInfo.expiresAt);

      storage.setItem('accessToken', authInfo.token);
    }
    setAuthState(authInfo);
  };

  const logout = (redirect = true) => {
    localStorage.removeItem(constants.TOKEN);
    localStorage.removeItem(constants.USER);
    localStorage.removeItem(constants.EXPIRES_AT);
    storage.removeItem('accessToken');

    setAuthInfo({ token: '', expiresAt: '', user: {} });
    if (redirect) {
      history.push('/account/login');
    }
  };

  const isAuthenticated = () => {
    if (!authState.token) {
      return false;
    }
    return new Date().getTime() < parseInt(authState.expiresAt);
  };

  const isAdmin = () => {
    return authState?.user?.roles.includes('admin');
  };

  return (
    <Provider
      value={{
        authState,
        setAuthState: (authInfo) => setAuthInfo(authInfo),
        logout,
        isAuthenticated,
        isAdmin,
      }}
    >
      {children}
    </Provider>
  );
};

export { AuthContext, AuthProvider };
