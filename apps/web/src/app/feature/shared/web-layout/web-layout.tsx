import React from 'react';
import Footer from '../footer/footer';
import Header from '../header/header';
import './web-layout.module.scss';

/* eslint-disable-next-line */
export interface WebLayoutProps {}

const WebLayout = ({ children }) => {
  return (
    <>
      <Header />
      <div className="container mx-auto min-h-screen">{children}</div>
      <Footer />
    </>
  );
};

export default WebLayout;
