import React from 'react';
import { Link } from 'react-router-dom';

const MainLogo = () => {
  return (
    <div className="text-3xl font-bold cursor-pointer">
      <Link className="text-primary-500" to="/">
        <span className="text-red-400 ">
          PixelSoft
        </span>
      </Link>
    </div>
  );
};

export default MainLogo;
