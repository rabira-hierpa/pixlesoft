import React from 'react';
import { Button } from 'antd';
import { NavLink } from 'react-router-dom';
import MainLogo from '../logo/logo';
import './header.module.scss';
/* eslint-disable-next-line */
export interface HeaderProps {}

const Header = (props: HeaderProps) => {
  return (
    <header className="lg:px-16 px-6 bg-white shadow-md flex flex-wrap items-center lg:py-0 py-2 space-x-8">
      <div className="lg:flex-none flex-1 flex">
        <MainLogo />
      </div>
      <label htmlFor="menu-toggle" className="pointer-cursor lg:hidden block">
        <svg
          className="fill-current text-gray-900"
          xmlns="http://www.w3.org/2000/svg"
          width="20"
          height="20"
          viewBox="0 0 20 20"
        >
          <title>menu</title>
          <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"></path>
        </svg>
      </label>
      <input className="hidden" type="checkbox" id="menu-toggle" />
      <div
        className={
          'hidden lg:flex-1 lg:flex lg:items-center justify-between lg:w-auto w-full'
        }
        id="menu"
      >
        <div className="">&nbsp;</div>
        <nav className="items-center pt-4 ml-4 flex justify-end">
          <ul className="lg:flex text-base space-x-4 text-gray-700 pb-4 lg:pt-0">
            <li>
              <NavLink
                className="top-menu flex items-center font-medium"
                to="/account/login"
                exact
                activeClassName="active"
              >
                <Button>Sign In</Button>
              </NavLink>
            </li>
            <li>
              <NavLink
                className="top-menu flex items-center font-medium"
                to="/account/register"
                exact
                activeClassName="active"
              >
                <Button type="primary">Sign Up</Button>
              </NavLink>
            </li>
          </ul>
        </nav>
      </div>
    </header>
  );
};

export default Header;
