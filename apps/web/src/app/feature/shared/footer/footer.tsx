import './footer.module.scss';
import dayjs from 'dayjs'
/* eslint-disable-next-line */
export interface FooterProps {}

export function Footer(props: FooterProps) {
  return (
    <div className="w-full grid p-10">
      <span className="text-center font-semibold">PixelSoft &copy; {dayjs().format('YYYY')}</span>
    </div>
  );
}

export default Footer;
