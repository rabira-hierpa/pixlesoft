import './landing-page.module.scss';

/* eslint-disable-next-line */
export interface LandingPageProps {}

export function LandingPage(props: LandingPageProps) {
  return (
    <div>
      <h1>Welcome to landing-page!</h1>
    </div>
  );
}

export default LandingPage;
