import React, { StrictMode } from 'react';
import * as ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { merge } from 'lodash-es';
import { environment } from './environments/environment';
import { IntlProvider } from 'react-intl';
import { ErrorBoundary } from 'react-error-boundary';
import { ErrorFallback } from '@pixelsoft/react/core';
import { AuthProvider } from './app/feature/shared/contexts/auth.context'
import locale_en from './lang/en_US.json';
import App from './app/app';
import 'antd/dist/antd.css';
import './styles/tailwind.dev.css';
const localStorageEnv = localStorage.getItem('env');
const env = localStorageEnv ? JSON.parse(localStorageEnv ?? '{}') : {};
window['__env'] = merge(environment, env);
const locale = navigator.language;
const languages = {
  en_US: locale_en,
};
ReactDOM.render(
  <ErrorBoundary FallbackComponent={ErrorFallback}>
    <BrowserRouter>
      <IntlProvider
        locale={locale}
        messages={languages[locale] ? languages[locale] : languages['en_US']}
      >
        <AuthProvider>
          <App />
        </AuthProvider>
      </IntlProvider>
    </BrowserRouter>
  </ErrorBoundary>,
  document.getElementById('root')
);
